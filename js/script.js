let container = document.querySelector('.content__container');
let modal = document.querySelector('.fixed-overlay');
let openModalBtn = document.querySelector('.header__button');
let closeModalBtn = document.querySelector('.modal__close-button');
let emptyContentTitle = document.querySelector('.content__empty');
let selectDoctor = document.getElementById('doctor');
let inputPurposeOfVisit = document.getElementById('purposeOfVisit');
let inputName = document.getElementById('name');
let inputDateOfVisit = document.getElementById('dateOfVisit');
let inputComments = document.getElementById('comments');
let inputLastVisit = document.getElementById('lastVisit');
let inputAge = document.getElementById('age');
let inputPressure = document.getElementById('pressure');
let inputMassIndex = document.getElementById('massIndex');
let inputCardiovascular = document.getElementById('cardiovascular');
let form = document.querySelector('form');
let submitButton = document.getElementById('submitButton');
let cards = [];

openModalBtn.onclick = function () {
	modal.style.display = 'block';
	let doctor = document.getElementById('doctor').value;
	showInputs(doctor);
};

closeModalBtn.onclick = function () {
	modal.style.display = 'none';
	document.querySelectorAll('.form__input').forEach((input) => {
		input.value = null;
	})
};

document.getElementById('modal').onclick = (e) => {
	if (e.target === e.currentTarget) {
		modal.style.display = 'none';
		document.querySelectorAll('.form__input').forEach((input) => {
			input.value = null;
		})
	}
};

document.getElementById('doctor').onchange = () => {
	let doctor = document.getElementById('doctor').value;
	showInputs(doctor);
};

form.onsubmit = function () {
	let doctor = document.getElementById('doctor').value;
	let newCard;
	switch (doctor) {
		case 'Стоматолог': newCard = new VisitToDentist();
			break;
		case 'Терапевт': newCard = new VisitToTherapist();
			break;
		case 'Кардиолог': newCard = new VisitToCardiologist();
			break;
	}
	cards.push(newCard);
	modal.style.display = 'none';
	clearInputs();
	newCard.createCard();
	return false
};

class Visit {
	constructor() {
		this.purpose = inputPurposeOfVisit.value;
		this.name = inputName.value;
		this.date = inputDateOfVisit.value;
		this.comment = inputComments.value;
		this.doctor = selectDoctor.value;
	}

	createCard() {
		let card = document.createElement('div');
		card.classList.add('card');
		card.innerHTML = `
		<button class="card__button-close">X</button>
		<p class="card__line">ФИО: ${this.name}</p>
		<p class="card__line">Доктор: ${this.doctor}</p>
		<button class="card__button-show">Показать больше</button>
	`;
		container.appendChild(card);
		let fullView = false;
		card.onclick = (e) => {
			if (e.target.classList.contains('card__button-close')) {
				this.deleteCard();
			}else if (e.target.classList.contains('card__button-show')) {
				if (!fullView) {
					for (let key in this) {
						switch (key) {
							case 'date':
								card.innerHTML += `<p class="card__line">Дата визита: ${this[key]}</p>`;
								break;
							case 'purpose':
								card.innerHTML += `<p class="card__line">Цель визита: ${this[key]}</p>`;
								break;
							case 'lastVisit':
								card.innerHTML += `<p class="card__line">Дата последнего визита: ${this[key]}</p>`;
								break;
							case 'age':
								card.innerHTML += `<p class="card__line">Возраст: ${this[key]}</p>`;
								break;
							case 'pressure':
								card.innerHTML += `<p class="card__line">Обычное давление: ${this[key]}</p>`;
								break;
							case 'massIndex':
								card.innerHTML += `<p class="card__line">Индекс массы тела: ${this[key]}</p>`;
								break;
							case 'cardiovascular':
								card.innerHTML += `<p class="card__line">Перенесенные заболевания сердечно-сосудистой системы: ${this[key]}</p>`;
								break;
							case 'comment':
								if (this.comment) card.innerHTML += `<p class="card__line">Комментарий: ${this[key]}</p>`;
								break;
						}
					}
					card.querySelector('.card__button-show').innerHTML = 'Свернуть';
					card.appendChild(card.querySelector('.card__button-show'));
					fullView = true;
				} else {
					card.innerHTML = `
				<button class="card__button-close">X</button>
				<p class="card__line">ФИО: ${this.name}</p>
				<p class="card__line">Доктор: ${this.doctor}</p>
				<button class="card__button-show">Показать больше</button>
				`;
					fullView = false;
				}
			}
		};
		card.onmousedown = this.drugAndDrop;
		localStorage.setItem('cards', JSON.stringify(cards));
		emptyContentTitle.style.display = 'none';
	}

	deleteCard() {
		cards.forEach((el, i) => {
			if (el === this) {
				cards.splice(i, 1);
				document.querySelectorAll('.card')[i].remove();
			}
		});
		localStorage.setItem('cards', JSON.stringify(cards));
		if (!document.querySelector('.card')) emptyContentTitle.style.display = 'block';
	}

	drugAndDrop(e) {
		let drugAndDrop = {};
		if (!e.target.classList.contains('card__button-close') && !e.target.classList.contains('card__button-show')) {
			let card = e.currentTarget;
			let cardIndex;
			document.querySelectorAll('.card').forEach((el, i) => {
				if (el === card) cardIndex = i;
			});
			let cardObj = cards[cardIndex];
			cards.splice(cardIndex, 1);
			drugAndDrop.shiftX = e.pageX - card.getBoundingClientRect().left + 10;
			drugAndDrop.shiftY = e.pageY - card.getBoundingClientRect().top + 10;
			card.style.position = 'absolute';
			card.style.left = e.pageX - drugAndDrop.shiftX + 'px';
			card.style.top = e.pageY - drugAndDrop.shiftY + 'px';
			card.style.zIndex = '10';
			let emptyArea = document.createElement('div');
			emptyArea.classList.add('card__empty-area');
			emptyArea.style.height = card.offsetHeight + 'px';
			container.insertBefore(emptyArea, card);
			document.onmousemove = function (e) {
				card.style.left = e.pageX - drugAndDrop.shiftX + 'px';
				card.style.top = e.pageY - drugAndDrop.shiftY + 'px';
				card.style.visibility = 'hidden';
				let mouseOverElem = document.elementFromPoint(e.pageX, e.pageY);
				let isCard = mouseOverElem.classList.contains('card');
				if (isCard) {
					if (!mouseOverElem.nextSibling || !mouseOverElem.nextSibling.classList.contains('card__empty-area')) {
						container.insertBefore(emptyArea, mouseOverElem.nextSibling);
					} else if (mouseOverElem.nextSibling.classList.contains('card__empty-area')) {
						container.insertBefore(emptyArea, mouseOverElem)
					}
				};
				card.style.visibility = 'visible';
			};
			document.onmouseup = function (e) {
				card.style.position = 'relative';
				card.style.top = 0;
				card.style.left = 0;
				card.style.zIndex = '1';
				container.insertBefore(card, emptyArea);
				let newCardIndex;
				for (let key in container.children) {
					if (container.children[key].tagName === 'DIV' && container.children[key].classList.contains('card__empty-area')) newCardIndex = key-3;
				}
				cards.splice(newCardIndex, 0, cardObj);
				localStorage.setItem('cards', JSON.stringify(cards));
				emptyArea.remove();
				document.onmousemove = null;
				document.onmouseup = null;
			}
		}
	}
}

class VisitToDentist extends Visit {
	constructor() {
		super();
		this.lastVisit = inputLastVisit.value;
	}
}

class VisitToCardiologist extends Visit {
	constructor() {
		super();
		this.age = inputAge.value;
		this.pressure = inputPressure.value;
		this.massIndex = inputMassIndex.value;
		this.cardiovascular = inputCardiovascular.value;
	}
}

class VisitToTherapist extends Visit {
	constructor() {
		super();
		this.age = inputAge.value;
	}
}

function showGeneralInputs() {
	clearInputs();
	document.querySelectorAll('.form__input').forEach((input) => {
		input.remove();
		input.style.display = 'block';
	});
	form.appendChild(inputPurposeOfVisit);
	form.appendChild(inputName);
	form.appendChild(inputDateOfVisit);
	form.appendChild(inputComments);
	form.appendChild(submitButton);
}

function showDentistInputs() {
	showGeneralInputs();
	form.insertBefore(inputLastVisit, inputComments);
}

function showTherapistInputs() {
	showGeneralInputs();
	form.insertBefore(inputAge, inputComments);
}

function showCardiologistInputs() {
	showGeneralInputs();
	form.insertBefore(inputAge, inputComments);
	form.insertBefore(inputPressure, inputComments);
	form.insertBefore(inputMassIndex, inputComments);
	form.insertBefore(inputCardiovascular, inputComments);
}

function showInputs(doctor) {
	switch (doctor) {
		case 'Стоматолог': showDentistInputs();
			break;
		case 'Терапевт': showTherapistInputs();
			break;
		case 'Кардиолог': showCardiologistInputs();
			break;
	}
}

function clearInputs() {
	document.querySelectorAll('.form__input').forEach((input) => {
		input.value = null;
	});
}

if (localStorage.getItem('cards')) {
	let cardsFromLS = JSON.parse(localStorage.getItem('cards'));
	cardsFromLS.forEach((el) => {
		let card;
		switch (el.doctor) {
			case 'Стоматолог': card = Object.assign(Object.create(VisitToDentist.prototype), el);
				break;
			case 'Терапевт': card = Object.assign(Object.create(VisitToTherapist.prototype), el);
				break;
			case 'Кардиолог': card = Object.assign(Object.create(VisitToCardiologist.prototype), el);
				break;
		}
		cards.push(card);
		card.createCard();
	});
	if (document.querySelector('.card')) emptyContentTitle.style.display = 'none';
}